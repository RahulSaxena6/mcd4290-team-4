"use strict";
function displayBooking(bookings, displayRefId) {
  let html = "";

  bookings.forEach((booking) => {
    // console.log(new Date(booking.startTime));
    html += `
    <tr>
        <td class="mdl-data-table__cell--non-numeric">${formatDateTime(
          booking.startTime
        )}</td>
        <td class="mdl-data-table__cell--non-numeric">${booking.taxi.type}</td>
        <td class="mdl-data-table__cell--non-numeric">${booking.stops[0].address.substring(
          0,
          booking.stops[0].address.indexOf(",")
        )}</td>
        <td class="mdl-data-table__cell--non-numeric">${booking.stops[
          booking.getNumberOfStops() - 1
        ].address.substring(
          0,
          booking.stops[booking.getNumberOfStops() - 1].address.indexOf(",")
        )}</td>
        <td>${booking.getNumberOfStops()}</td>
        <td>$${parseFloat(booking.fare).toFixed(2)}</td>
        <td>${booking.distance} KM</td>               
        <td><a href="#" onclick="viewDetails('${
          booking.bookingId
        }')">View</a></td>               
        <td>${
          new Date(booking.startTime).getDate() >= new Date().getDate() + 1
            ? `<a href="#" onclick="deleteBooking('${booking.bookingId}')">Delete</a>`
            : ``
        }</td>               
    </tr>
    
    `;
  });
  document.getElementById(displayRefId).innerHTML = html;
}

function viewDetails(bookingId) {
  let tripIndex = trips.getTripIndexById(bookingId);
  updateLSData(KEY_TRIP_INDEX, tripIndex);
  window.location = "viewDetailedPage.html";
}

function deleteBooking(bookingId) {
  if (confirm("Are you sure you want to cancel this trip?")) {
    let tripIndex = trips.getTripIndexById(bookingId);
    let trip = trips.getTripIndexById(tripIndex);
    updateTaxiAvailability(trip.taxi.rego, true);
    trips.removeBooking(tripIndex);
    updateLSData(KEY_TRIPS, trips);
    updateUI();
  }
}

function updateUI() {
  let ongoingBookings = trips.filterTrips(
    (t) => new Date(t.startTime).getDate() == new Date().getDate()
  );

  let futureBookings = trips.filterTrips(
    (t) => new Date(t.startTime).getDate() >= new Date().getDate() + 1
  );

  let pastBookings = trips.filterTrips(
    (t) => new Date(t.startTime).getDate() < new Date().getDate()
  );
  displayBooking(ongoingBookings, "ongoingBookings");
  displayBooking(futureBookings, "futureBookings");
  displayBooking(pastBookings, "pastBookings");
}

updateUI();
