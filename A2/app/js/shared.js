"use strict";

class Taxi {
  constructor(rego, type, available) {
    this.rego = rego;
    this.available = available;
    this.type = type;
  }

  fromData(data) {
    let taxi = new Taxi();
    taxi.rego = data.rego;
    taxi.available = data.available;
    taxi.type = data.type;
    return taxi;
  }
}

class Stop {
  constructor(id, lat, lng, address) {
    this._stopId = id;
    this._lat = lat;
    this._lng = lng;
    this._address = address;
  }

  get stopId() {
    return this._stopId;
  }

  set stopId(stopId) {
    this._stopId = stopId;
  }

  get lat() {
    return this._lat;
  }

  get lng() {
    return this._lng;
  }

  get address() {
    return this._address;
  }

  set lat(lat) {
    this._lat = lat;
  }

  set lng(lng) {
    this._lng = lng;
  }

  set address(address) {
    this._address = address;
  }
  fromData(data) {
    let stop = new Stop();
    stop._stopId = data._stopId;
    stop._lat = data._lat;
    stop._lng = data._lng;
    stop._address = data._address;
    return stop;
  }
}

class Booking {
  constructor() {
    this._bookingId = generateUniqueId();
    this._startTime = "";
    this._stops = [];
    this._taxi = new Taxi();
    this._distance = 0;
    this._fare = 0;
  }

  getNumberOfStops() {
    return this._stops.length;
  }

  get bookingId() {
    return this._bookingId;
  }

  set bookingId(bookingId) {
    this._bookingId = bookingId;
  }

  set startTime(startTime) {
    this._startTime = startTime;
  }

  get startTime() {
    return this._startTime;
  }

  get stops() {
    return this._stops;
  }

  set stops(stops) {
    this._stops = stops;
  }

  get taxi() {
    return new Taxi().fromData(this._taxi);
  }

  set taxi(taxi) {
    this._taxi = taxi;
  }

  get distance() {
    return this._distance;
  }

  set distance(distance) {
    this._distance = distance;
  }

  get fare() {
    return this._fare;
  }

  set fare(fare) {
    this._fare = fare;
  }

  fromData(data) {
    // console.log(data);

    let booking = new Booking();
    booking._bookingId = data._bookingId;
    booking._startTime = data._startTime;
    booking._taxi = new Taxi().fromData(data._taxi);
    booking._fare = data._fare;
    booking._distance = data._distance;
    let stops = [];
    data._stops.forEach((s) => {
      stops.push(new Stop().fromData(s));
    });
    booking._stops = stops;

    return booking;
  }
}

class Trips {
  constructor() {
    //trips contain a list of bookings
    this._trips = [];
  }

  updateTrip(index, newTrip) {
    this._trips[index] = newTrip;
  }

  filterTrips(condition) {
    return this._trips.filter(condition);
  }

  addBooking(booking) {
    this._trips.push(booking);
  }

  getTripByIndex(index) {
    return new Booking().fromData(this._trips[index]);
  }

  getTripIndexById(id) {
    return this._trips.findIndex((t) => t._bookingId == id);
  }

  removeBooking(index) {
    this._trips.splice(index, 1);
  }

  getLength() {
    return this._trips.length;
  }

  fromData(data) {
    let tripsArray = [];
    data._trips.forEach((t) => {
      let booking = new Booking().fromData(t);
      tripsArray.push(booking);
    });
    this._trips = tripsArray;
  }
}

function generateUniqueId() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

function showSnackBar(id) {
  // Get the snackbar DIV
  var x = document.getElementById(id);

  // Add the "show" class to DIV
  x.className = "show";

  // After 3 seconds, remove the show class from DIV
  setTimeout(function () {
    x.className = x.className.replace("show", "");
  }, 3000);
}

function formatDateTime(dateTimeString) {
  let dateTime = new Date(dateTimeString);
  return dateTime.toLocaleDateString() + " " + dateTime.toLocaleTimeString();
}

const KEY_TRIPS = "trips";
const KEY_TRIP_INDEX = "trip_index";

let trips = new Trips();
let booking = new Booking();
if (checkLSData(KEY_TRIPS)) {
  let data = retrieveLSData(KEY_TRIPS);
  trips.fromData(data);
  // console.log(trips);
}
