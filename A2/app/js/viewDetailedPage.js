"use strict";

mapboxgl.accessToken =
  "pk.eyJ1IjoibWNkNDI5MC10aWFubmluZ3lhbmciLCJhIjoiY2tudHV4OTVkMDV0NTJ1cGp0eHFmYjdmZyJ9.eBWm2cEOiI-UOVxvIKOftA";

let map1 = new mapboxgl.Map({
  container: "map1", // container ID
  style: "mapbox://styles/mapbox/streets-v11", // style URL
  center: [-74.5, 40], // starting position [lng, lat]
  zoom: 9, // starting zoom
});

function displayTripDetails(trip) {
  // console.log(trip.taxi)
  let stopsHtml = "";
  trip.stops.forEach((stop) => {
    stopsHtml += `<li>${stop.address}</li>`;
  });

  let html = ` <tbody>
                  <tr>
                    <th class="mdl-data-table__cell--non-numeric">Start Time</td>
                    <td>${formatDateTime(trip.startTime)}</td>
                  </tr>
                  <tr>
                    <th class="mdl-data-table__cell--non-numeric" style="vertical-align: middle;">Stops</td>
                    <td style="text-align:left">${stopsHtml}</td>
                  </tr>
                  <tr>
                    <th class="mdl-data-table__cell--non-numeric" style="vertical-align: middle;">Taxi Type</td>
                    <td>
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <select class="mdl-textfield__input" id="taxiType" name="taxiType" onchange="onTaxiTypeChange();">
                          <option value="Car">Car</option>
                          <option value="Van">Van</option>
                          <option value="SUV">SUV</option>
                          <option value="Minibus">Minibus</option>
                        </select>
                      </div>                     
                    </td>
                  
                  </tr>
                  <tr>
                    <th class="mdl-data-table__cell--non-numeric">Total Distance</td>
                    <td>${trip.distance} KM</td>
                  </tr>
                  <tr>
                    <th class="mdl-data-table__cell--non-numeric">Total Fare</td>
                    <td id="totalFare">$${parseFloat(trip.fare).toFixed(2)}</td>
                    <td>
                        <div style="margin: 10px;">
                          <button 
                          id="btnCancelBooking"
                          class=" 
                            mdl-button mdl-js-button 
                            mdl-button--raised mdl-button--colored 
                          " 
                          onclick="cancelBooking()"
                        > 
                          Cancel 
                        </button> 
                    
                          <button 
                          id="btnUpdateBooking"
                          class=" 
                            mdl-button mdl-js-button 
                            mdl-button--raised mdl-button--colored 
                          " 
                          onclick="updateBooking()"
                        > 
                          Update 
                        </button> 
                        </div>
                    </td>
                  </tr>
                 
                </tbody>`;

  document.getElementById("tripDetails").innerHTML = html;
  selectElement("taxiType", trip.taxi.type);

  if (new Date(trip.startTime).getDate() < new Date().getDate() + 1) {
    document.getElementById("taxiType").style.pointerEvents = "none";
    document.getElementById("btnUpdateBooking").style.visibility = "hidden";
    document.getElementById("btnCancelBooking").style.visibility = "hidden";
  }
}

function selectElement(id, valueToSelect) {
  let element = document.getElementById(id);
  element.value = valueToSelect;
}

function onTaxiTypeChange() {
  let taxiType = document.getElementById("taxiType").value;
  let taxi = getFirstAvailableTaxi(taxiType);
  if (taxi != undefined) {
    document.getElementById("btnUpdateBooking").style.visibility = "visible";
    let fare = getTotalFare(taxiType, trip.distance, trip.startTime);
    document.getElementById("totalFare").innerText = "$" + fare.toFixed(2);
    trip.taxi = taxi;
    trip.fare = fare;
  } else {
    showSnackBar("snackbar");
    document.getElementById("btnUpdateBooking").style.visibility = "hidden";
  }
}

function updateBooking() {
  trips.updateTrip(tripIndex, trip);
  updateLSData(KEY_TRIPS, trips);
  alert("You booking is updated successfully.");
}

function cancelBooking() {
  let tripIndex = retrieveLSData(KEY_TRIP_INDEX);
  if (tripIndex != undefined) {
    if (confirm("Do you really want to cancel this booking?")) {
      let trip = trips.getTripByIndex(tripIndex);
      console.log(trip);
      updateTaxiAvailability(trip.taxi.rego, true);
      trips.removeBooking(tripIndex);
      updateLSData(KEY_TRIPS, trips);
      window.location = "index.html";
    }
  }
}

function displayRoute(trip) {
  let stops = trip.stops;
  let features = [];
  let geojson = {
    type: "geojson",
    data: {
      type: "FeatureCollection",
      features: features,
    },
  };

  for (let i = 0; i < trip.getNumberOfStops() - 1; i++) {
    let feature = {
      type: "Feature",
      properties: {},
      geometry: {
        type: "LineString",
        coordinates: [
          [stops[i].lng, stops[i].lat],
          [stops[i + 1].lng, stops[i + 1].lat],
        ],
      },
    };
    features.push(feature);
  }

  geojson.data.features = features;

  if (typeof map1.getLayer("routes") !== "undefined") {
    map1.removeLayer("routes").removeSource("routes");
  }
  map1.addSource("routes", geojson);
  map1.addLayer({
    id: "routes",
    type: "line",
    source: "routes",
    paint: {
      "line-color": "#888",
      "line-width": 3,
    },
  });
}

function displayMarkers(trip) {
  let stops = trip.stops;

  map1.flyTo({
    center: [stops[0].lng, stops[0].lat],
    essential: true,
    zoom: 13,
  });

  for (let i = 0; i < trip.getNumberOfStops(); i++) {
    let title = "";
    if (i == 0) {
      title = "Pick-up location - Stop " + (i + 1);
    } else if (i == trip.getNumberOfStops() - 1) {
      title = "Drop-off location - Stop " + (i + 1);
    } else {
      title = "Stop " + (i + 1);
    }
    let popup = new mapboxgl.Popup({ closeOnClick: false })
      .setLngLat([stops[i].lng, stops[i].lat])
      .setHTML(`<strong>${title}</strong><br/><p>${stops[i].address}</p>`)
      .addTo(map1);

    let marker = new mapboxgl.Marker({ draggable: false })
      .setLngLat([stops[i].lng, stops[i].lat])
      .addTo(map1)
      .setPopup(popup);
  }
}

//Code run on page load
let tripIndex = retrieveLSData(KEY_TRIP_INDEX);
let trip = trips.getTripByIndex(tripIndex);
// console.log(data)
displayTripDetails(trip);
map1.on("load", function () {
  displayMarkers(trip);
  displayRoute(trip);
});
