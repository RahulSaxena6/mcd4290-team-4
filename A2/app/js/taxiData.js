let taxiList = [
  { rego: "VOV-887", type: "Car", available: true },
  { rego: "OZS-293", type: "Van", available: false },
  { rego: "WRE-188", type: "SUV", available: true },
  { rego: "FWZ-490", type: "Car", available: true },
  { rego: "NYE-874", type: "SUV", available: true },
  { rego: "TES-277", type: "Car", available: false },
  { rego: "GSP-874", type: "SUV", available: false },
  { rego: "UAH-328", type: "Minibus", available: true },
  { rego: "RJQ-001", type: "SUV", available: false },
  { rego: "AGD-793", type: "Minibus", available: false },
];

function updateTaxiAvailability(rego, available) {
  for (let i = 0; i < taxiList.length; i++) {
    if (taxiList[i].rego == rego) {
      taxiList[i].available = available;
      break;
    }
  }
}

function getFirstAvailableTaxi(type) {
  // let futureBookings = trips.filterTrips(
  //   (t) => new Date(t.startTime).getDate() >= new Date().getDate() + 1
  // );
  // let isAvailableOnBookingDate = futureBookings.filter(b=>b.type == type).map(t => new Date(t.startTime).toLocaleDateString()).includes(new Date(bookingTime).toLocaleDateString());
  return taxiList.find((t) => t.available && t.type == type);
}

function getVehicleLevy(type) {
  let levy = 1.1; //commercial passenger vehicle levy
  if (type == "SUV") {
    levy += 3.5;
  } else if (type == "Van") {
    levy += 6;
  } else if (type == "Minibus") {
    levy += 10;
  }
  return levy;
}

function getTotalFare(type, distance, startTime) {
  let flatFare = 4.3;
  let distanceFareRate = 1.622;
  let nighLevyPercentage = 0.2;
  let hour = new Date(startTime).getHours();
  let totalFare = flatFare + distanceFareRate * distance + getVehicleLevy(type);
  if (hour > 17 && hour < 21) {
    totalFare *= 1 + nighLevyPercentage;
  }
  return totalFare;
}
