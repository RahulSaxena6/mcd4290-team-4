"use strict";

let stopIndex = 0;
let address = "";
let bookingStops = [];
let totalDistance = 0; //unit is meter
let totalDuration = 0; //unit is millisec

mapboxgl.accessToken =
  "pk.eyJ1IjoicmFodWxzYXhlbmEiLCJhIjoiY2tzcTEwcnAxMDhqcDJ4cHUxYjNpZHprYSJ9.FV-8WZXr_4KXXZjrudliHg";
let map = new mapboxgl.Map({
  container: "map", // container ID
  style: "mapbox://styles/mapbox/streets-v11", // style URL
  center: [145.128565, -37.915863], // starting position [lng, lat]
  zoom: 15, // starting zoom
});

function addStopNode(stopIndex) {
  let stopsRef = document.getElementById("stops");
  let stopNode = document.createElement("div");
  stopNode.innerHTML = `
                        <form action="#" id="stop${stopIndex}form"> 
                            <div 
                              class=" 
                                mdl-textfield mdl-js-textfield 
                                mdl-textfield--floating-label 
                              " 
                            > 
                              <input 
                                class="mdl-textfield__input stop-node" 
                                type="text" 
                                id="stop${stopIndex}" 
                                name = "stop${stopIndex}"
                              /> 
                              <label class="mdl-textfield__label" for="stop${stopIndex}" 
                                ></label 
                              > 
                              <label class="mdl-button mdl-js-button mdl-button--icon" for="stop${stopIndex}" style="margin-left: 300px;">
                                <i class="material-icons" onclick="searchStop('stop${stopIndex}')">search</i>
                              </label>
                              <label class="mdl-button mdl-js-button mdl-button--icon" for="stop${stopIndex}" style="margin-left: 330px;">
                                <i class="material-icons" onclick="removeStopNode('stop${stopIndex}')">delete</i>
                              </label>
                              ${
                                stopIndex == 0
                                  ? `<label class="mdl-button mdl-js-button mdl-button--icon" for="stop${stopIndex}" style="margin-left: 360px;">
                                <i class="material-icons" id="btnCurrentLoaction">my_location</i>
                              </label>`
                                  : ``
                              }
                            </div> 
                        </form>    
                       `;

  stopsRef.appendChild(stopNode);
}

window.onload = function () {
  document
    .getElementById("btnCurrentLoaction")
    .addEventListener("click", function (e) {
      getUserCurrentLocationUsingGeolocation(onCurrentLocationResponse);
    });
};

function onCurrentLocationResponse(lat, lng) {
  sendWebServiceRequestForReverseGeocoding(lat, lng, "onLocationChanged");
  setTimeout(function () {
    sendWebServiceRequestForForwardGeocoding(address, "onAddressResponse");
  }, 1000);
}

// document.getElementById("btnAddStop").addEventListener("click", (e) => {
//   addStopNode(stopIndex);
//   stopIndex += 1;
// });

function displayRoute(stops) {
  let features = [];
  let geojson = {
    type: "geojson",
    data: {
      type: "FeatureCollection",
      features: features,
    },
  };

  for (let i = 0; i < stops.length - 1; i++) {
    let feature = {
      type: "Feature",
      properties: {},
      geometry: {
        type: "LineString",
        coordinates: [
          [stops[i]._lng, stops[i]._lat],
          [stops[i + 1]._lng, stops[i + 1]._lat],
        ],
      },
    };
    features.push(feature);
  }

  geojson.data.features = features;

  if (typeof map.getLayer("routes") !== "undefined") {
    map.removeLayer("routes").removeSource("routes");
  }
  map.addSource("routes", geojson);
  map.addLayer({
    id: "routes",
    type: "line",
    source: "routes",
    paint: {
      "line-color": "#888",
      "line-width": 3,
    },
  });
}

function removeStopNode(id) {
  // let index = parseInt(id.substring(4, 5));
  console.log(id);
  bookingStops.splice(
    bookingStops.findIndex((b) => b._stopId == id),
    1
  );
  let stop = document.getElementById(id + "form");
  stop.parentNode.removeChild(stop);
  displayRoute(bookingStops);
  console.log(bookingStops);
}

function searchStop(id) {
  console.log(id);
  let address = document.getElementById(id).value;
  sendWebServiceRequestForForwardGeocoding(address, "onAddressResponse");
}

function onAddressResponse(response) {
  let stop = new Stop();
  stop.stopId = "stop" + (stopIndex - 1);
  console.log(stop.stopId);
  let result = response.results[0];
  if (result !== undefined) {
    let longitude = result.geometry.lng;
    let latitude = result.geometry.lat;
    address = result.formatted;

    map.flyTo({
      center: [longitude, latitude],
      essential: true,
      zoom: 15,
    });

    setTimeout(function () {
      // let stopId = "stop" + stopIndex;

      let popup = new mapboxgl.Popup({ closeOnClick: false }).setLngLat([
        longitude,
        latitude,
      ]);
      popup
        .setHTML(
          `<p>${address}</p>
            <button style="float:right" class='mdl-button mdl-js-button mdl-button--raised mdl-button--colored button-add-stop' onclick="addStopToBooking('${stop.stopId}',${latitude}, ${longitude}, '${address}')">Add</button>`
        )
        .addTo(map);

      let marker = new mapboxgl.Marker({ draggable: true })
        .setLngLat([longitude, latitude])
        .addTo(map)
        .setPopup(popup);

      document.getElementById("stop" + (stopIndex - 1)).value = address;

      function onDragEnd() {
        let lngLat = marker.getLngLat();
        let lat = lngLat.lat;
        let lng = lngLat.lng;
        sendWebServiceRequestForReverseGeocoding(lat, lng, "onLocationChanged");

        setTimeout(function () {
          popup.setHTML(
            `<p>${address}</p>
              <button style="float:right" class='mdl-button mdl-js-button mdl-button--raised mdl-button--colored button-add-stop' onclick="addStopToBooking('${stop.stopId}',${lat}, ${lng}, '${address}')">Add</button>`
          );
          document.getElementById("stop" + (stopIndex - 1)).value = address;
        }, 2000);
      }

      marker.on("dragend", onDragEnd);
    }, 1000);
  } else {
    showSnackBar("snackbar");
  }
}

function addStopToBooking(id, lat, lng, address, source = "") {
  console.log(id);
  let stop = new Stop(id, lat, lng, address);

  let index = bookingStops.findIndex((b) => b._stopId == stop.stopId);

  if (index != -1) {
    bookingStops[index] = stop;
  } else {
    bookingStops.push(stop);
  }
  displayRoute(bookingStops);
  console.log(bookingStops);

  document.querySelectorAll(".button-add-stop").forEach((s) => {
    s.style.visibility = "hidden";
  });

  if (source == "map") {
    if (index == -1) {
      addStopNode(stopIndex);
      stopIndex += 1;
    } else {
      updateStopNode(index);
    }
  } else {
    addStopNode(stopIndex);
    stopIndex += 1;
  }
}

function updateStopNode(index) {
  console.log(index);
  document.querySelectorAll(".stop-node").forEach((stop) => {
    if (stop.id == "stop" + index) {
      console.log(stop.id);
      stop.value = address;
    }
  });
}

function onLocationChanged(response) {
  address = response.results[0].formatted;
}

function getDistanceAndDurationForTrip(stopsArray) {
  for (let i = 0; i < stopsArray.length - 1; i++) {
    let stop1 = stopsArray[i];
    let stop2 = stopsArray[i + 1];
    sendXMLRequestForRoute(
      stop1.lat,
      stop1.lng,
      stop2.lat,
      stop2.lng,
      onDistanceResponse
    );
  }
}

function onDistanceResponse(response) {
  // console.log(response)
  let distance = response.routes[0].distance;
  let duration = response.routes[0].duration;
  totalDistance += distance;
  totalDuration += duration;
}

function confirmBooking() {
  let startTime = document.getElementById("startTime").value;
  let taxiType = document.getElementById("taxiTypeList").value;
  let taxi = getFirstAvailableTaxi(taxiType);

  if (startTime == "") {
    alert("please select a start time");
  } else if (taxiType == "") {
    alert("Please select a taxi type;");
  } else if (taxi == undefined) {
    alert("No taxi available under this type");
  } else if (bookingStops.length < 2) {
    alert("Please set at leat two locations.");
  } else {
    getDistanceAndDurationForTrip(bookingStops);

    setTimeout(function () {
      booking.fare = getTotalFare(
        taxiType,
        totalDistance / 1000.0,
        startTime
      ).toFixed(2);
      booking.startTime = startTime;
      booking.taxi = taxi;
      booking.stops = bookingStops;
      booking.distance = parseFloat(totalDistance / 1000.0).toFixed(2);
      if (confirm(getBookingSummary(booking))) {
        updateTaxiAvailability(taxi.rego, false);
        trips.addBooking(booking);
        updateLSData(KEY_TRIPS, trips);
        bookingStops = [];
        updateLSData(KEY_TRIP_INDEX, trips.getLength() - 1);
        window.location = "viewDetailedPage.html";
      }
    }, 1000);
  }
}

function getBookingSummary(booking) {
  let text =
    "You booking summary is as follows: \nStart Time: " +
    booking.startTime +
    "\nPick-up location: " +
    booking.stops[0].address +
    "\nDrop-off location: " +
    booking.stops[booking.getNumberOfStops() - 1].address +
    "\nEstimated distance: " +
    booking.distance +
    "KM\nEstimated fare: $" +
    booking.fare;
  return text;
}

if (stopIndex == 0) {
  addStopNode(stopIndex);
  stopIndex += 1;
}

function toISOLocal(d) {
  var z = (n) => ("0" + n).slice(-2);
  var zz = (n) => ("00" + n).slice(-3);
  var off = d.getTimezoneOffset();
  var sign = off < 0 ? "+" : "-";
  off = Math.abs(off);
  return (
    d.getFullYear() +
    "-" +
    z(d.getMonth() + 1) +
    "-" +
    z(d.getDate()) +
    "T" +
    z(d.getHours()) +
    ":" +
    z(d.getMinutes()) +
    ":" +
    z(d.getSeconds()) +
    "." +
    zz(d.getMilliseconds()) +
    sign +
    z((off / 60) | 0) +
    ":" +
    z(off % 60)
  );
}

//block past dates
document
  .getElementById("startTime")
  .setAttribute("min", toISOLocal(new Date()).substring(0, 16));

map.on("style.load", function () {
  map.on("click", function (e) {
    let stop = new Stop();
    stop.stopId = "stop" + (stopIndex - 1);
    let latitude = e.lngLat.lat;
    let longitude = e.lngLat.lng;
    sendWebServiceRequestForReverseGeocoding(
      latitude,
      longitude,
      "onLocationChanged"
    );
    setTimeout(function () {
      let popup = new mapboxgl.Popup({ closeOnClick: false }).setLngLat([
        longitude,
        latitude,
      ]);
      popup
        .setHTML(
          `<p>${address}</p>
                <button style="float:right" class='mdl-button mdl-js-button mdl-button--raised mdl-button--colored button-add-stop' onclick="addStopToBooking('${stop.stopId}',${latitude}, ${longitude}, '${address}','map')">Add</button>`
        )
        .addTo(map);

      let marker = new mapboxgl.Marker({ draggable: true })
        .setLngLat([longitude, latitude])
        .addTo(map)
        .setPopup(popup);

      document.getElementById("stop" + (stopIndex - 1)).value = address;

      function onDragEnd() {
        let lngLat = marker.getLngLat();
        let lat = lngLat.lat;
        let lng = lngLat.lng;
        sendWebServiceRequestForReverseGeocoding(lat, lng, "onLocationChanged");

        setTimeout(function () {
          popup.setHTML(
            `<p>${address}</p>
                  <button style="float:right" class='mdl-button mdl-js-button mdl-button--raised mdl-button--colored button-add-stop' onclick="addStopToBooking('${stop.stopId}',${lat}, ${lng}, '${address}', 'map')">Add</button>`
          );
          // document.getElementById("stop" + (stopIndex - 1)).value = address;
        }, 1500);
      }

      marker.on("dragend", onDragEnd);
    }, 1000);
  });
});
