/* Write your function for implementing the Bi-directional Search algorithm here */


function bidirectionalSearchTest(array) {
    let counter = 0;
    let lo = 0;
    let hi = array.length;
    let middle = Math.floor(lo + (hi - lo) / 2);

    while (counter <= array.length / 2) {
        if (array[middle - counter].emergency) {
            return array[middle - counter].address;
        } else if (array[middle + counter].emergency) {
            return array[middle + counter].address;
        } else {
        }

        counter++;
    }
}